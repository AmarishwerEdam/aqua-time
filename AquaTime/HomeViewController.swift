//
//  HomeViewController.swift
//  AquaTime
//
//  Created by Champ on 13/04/17.
//  Copyright © 2017 Edam,Amarishwer. All rights reserved.
//

import UIKit
import UserNotifications

class HomeViewController: UITabBarController {

    // MARK: - View Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        navigationController?.isNavigationBarHidden = true

        // This is only for testing local notifications...
        // If dev want to test local notification then uncomment below all lines...
        
//        removeAllScheduleNotification()
//        let delegate = UIApplication.shared.delegate as? AppDelegate
//        delegate?.scheduleNotification(timeInterval: 1)
    }
    
    // MARK: - Private Methods

    func removeAllScheduleNotification() -> Void {
        let center = UNUserNotificationCenter.current()
        center.removeAllDeliveredNotifications() // To remove all delivered notifications
        center.removeAllPendingNotificationRequests()
    }
    
    // MARK: - UITabBarControllerDelegate methods
    
    func tabBarController(_ tabBarController: UITabBarController, shouldSelect viewController: UIViewController) -> Bool {
        // NOTE: By default UITabBarController will cause the currently selected tab to redisplay if is selected again
        return tabBarController.selectedViewController != viewController
    }

}
