//
//  IntegerField.swift
//  AquaTime
//
//  Created by Edam,Amarishwer on 4/1/17.
//  Copyright © 2017 Edam,Amarishwer. All rights reserved.
//

import UIKit

class IntegerField: UITextField {
    
    var integer: Int {
        return string.digits.integer
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        addTarget(self, action: #selector(editingChanged), for: .editingChanged)
        keyboardType = .numberPad
        textAlignment = .right
        editingChanged()
    }
    
    func editingChanged() {
        text = Formatter.decimal.string(from: integer as NSNumber)
    }
    
}

extension NumberFormatter {
    convenience init(numberStyle: NumberFormatter.Style) {
        self.init()
        self.numberStyle = numberStyle
    }
    
}

extension Sequence where Iterator.Element == UnicodeScalar {
    var string: String { return String(String.UnicodeScalarView(self)) }
}

struct Formatter {
    static let decimal = NumberFormatter(numberStyle: .decimal)
}

extension UITextField {
    var string: String { return text ?? "" }
}

extension String {
    private static var digitsPattern = UnicodeScalar("0")..."9"
    var digits: String {
        return unicodeScalars.filter { String.digitsPattern ~= $0 }.string
    }
    var integer: Int { return Int(self) ?? 0 }
}
