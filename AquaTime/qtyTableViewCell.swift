//
//  qtyTableViewCell.swift
//  AquaTime
//
//  Created by Champ on 16/04/17.
//  Copyright © 2017 Edam,Amarishwer. All rights reserved.
//

import UIKit

class qtyTableViewCell: UITableViewCell {

    static let identifier = "qtyTableViewCell"
    static let nibName = "qtyTableViewCell"
    
    @IBOutlet weak var labelqty: UILabel!
    @IBOutlet weak var labelDate: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
