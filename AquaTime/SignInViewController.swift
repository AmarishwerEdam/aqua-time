//
//  SignInViewController.swift
//  AquaTime
//
//  Created by Champ on 12/04/17.
//  Copyright © 2017 Edam,Amarishwer. All rights reserved.
//

import UIKit
import Parse

class SignInViewController: UIViewController,UITextFieldDelegate {
    
    // Here we set some UI var...
    
    @IBOutlet var txtEmail: UITextField!
    @IBOutlet var txtPassword: UITextField!
    
    @IBOutlet var buttonLogin: UIButton!
    @IBOutlet var buttonSignUp: UIButton!
    @IBOutlet var buttonForgetPassword: UIButton!
    
    // MARK: - View Life Cycle
    
    override func viewDidLoad() {
        // Do any additional setup after loading the view.
        super.viewDidLoad()
        
        navigationItem.hidesBackButton = true
        title = "Login Screen"
        
        setUIPart()
        
        self.hideKeyboardWhenTappedAround()
    }
    
    // MARK: - Setup UI
    
    func setUIPart() {
        buttonLogin.layer.cornerRadius = 5
        buttonLogin.layer.masksToBounds = true
        buttonLogin.layer.borderColor = UIColor.white.cgColor
        buttonLogin.layer.borderWidth = 1
        
        
        buttonSignUp.layer.cornerRadius = 5
        buttonSignUp.layer.masksToBounds = true
        buttonSignUp.layer.borderColor = UIColor.white.cgColor
        buttonSignUp.layer.borderWidth = 1
        
        buttonForgetPassword.layer.cornerRadius = 5
        buttonForgetPassword.layer.masksToBounds = true
        buttonForgetPassword.layer.borderColor = UIColor.white.cgColor
        buttonForgetPassword.layer.borderWidth = 1
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Action Method
    
    // User Sign Up method
    @IBAction func signUp(_ sender: AnyObject) {
        // Before excute api we validate all data is correct format...
        if (txtEmail?.text?.isValidEmail)!, (txtPassword.text?.characters.count)! > 0 {

            // For this progress HUD we're adding external lib...
            KRProgressHUD.show()
            
            let user =  PFUser()
            user.username = txtEmail?.text!
            user.password = txtPassword?.text!
            
            // Signing up using the Parse API
            user.signUpInBackground( block: {
                (success, error) -> Void in
                
                KRProgressHUD.dismiss()
                if let error1 = error as NSError? {
                    if let errorString = error1.userInfo["error"] as? String {
                        // In case something went wrong, use errorString to get the error
                        // For this progress HUD we're adding external lib...
                        Toast(text: errorString).show()
                        return
                    }
                    Toast(text: "Something has gone wrong!\n \(error1.userInfo["error"]!)").show()
                } else {
                    // Everything went okay
                    Toast(text: "Registration was successful.").show()
                    UserDefaults.standard.set(true, forKey: "LoggedIn")
                    self.performSegue(withIdentifier: "LoggedIn", sender: nil)
                }
            })
        } else {
            Toast(text: "Please check your email or password text field!!!").show()
        }
    }
    
    // Login method
    @IBAction func loginMethod(_ sender: AnyObject){
        // Before excute api we validate all data is correct format...
        if (txtEmail?.text?.isValidEmail)!, (txtPassword.text?.characters.count)! > 0 {
            KRProgressHUD.show()
            
            PFUser.logInWithUsername(inBackground: txtEmail.text!, password: txtPassword.text!, block:{(user, error) -> Void in
                
                KRProgressHUD.dismiss()
                if error != nil{
                    Toast(text: (error?.localizedDescription)!).show()
                }
                else {
                    // Everything went alright here
                    // Navigate to LoggedIn screen
                    self.performSegue(withIdentifier: "LoggedIn", sender: nil)
                }
            })
        } else {
            // If all data not in proper maner then we show toast to user...
            Toast(text: "Please check your email or password text field!!!").show()
        }
    }
    
    // Forget Password Method
    @IBAction func forgetPassword(_ sender: AnyObject) {
        
        let alertController = UIAlertController(title: "Alert", message: "Please enter your mail id.", preferredStyle: .alert)
        
        let saveAction = UIAlertAction(title: "Submit", style: .default, handler: {
            alert -> Void in
            
            let targetAmount = alertController.textFields![0] as UITextField
            
            // Validate email is correct format...
            if (targetAmount.text?.isValidEmail)! {
                KRProgressHUD.show()
                
                // Excute api for forget password...
                PFUser.requestPasswordResetForEmail(inBackground: targetAmount.text!, block: { (succeeded: Bool, error: Error?) -> Void in
                    if error == nil {
                        if succeeded { // SUCCESSFULLY SENT TO EMAIL
                            Toast(text: "Reset email sent to your inbox").show()
                        }
                        else {
                            // SOME PROBLEM OCCURED
                            Toast(text: "some probem oocured").show()
                        }
                    }
                    else { //ERROR OCCURED, DISPLAY ERROR MESSAGE
                        print(error.debugDescription);
                    }
                    KRProgressHUD.dismiss()
                });
                
            } else {
                // If email is not in format then show toast to user...
                Toast(text: "Please check your email or password text field!!!").show()
            }
        })
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .default, handler: {
            (action : UIAlertAction!) -> Void in
            
        })
        
        alertController.addTextField { (textField : UITextField!) -> Void in
            textField.placeholder = "Enter email id"
        }
        
        alertController.addAction(saveAction)
        alertController.addAction(cancelAction)
        
        self.present(alertController, animated: true, completion: nil)
    }
}
