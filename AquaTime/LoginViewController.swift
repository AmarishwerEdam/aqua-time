//
//  LoginViewController.swift
//  AquaTime
//
//  Created by Goteti,Santosh Ravi Teja on 4/1/17.
//  Copyright © 2017 Edam,Amarishwer. All rights reserved.
//

import UIKit
import CoreData
import Parse

class LoginViewController: UIViewController, UIPickerViewDelegate, UITextFieldDelegate, UIPickerViewDataSource{
    
    // create global constant obj for core data manage object context....
    let managedObjectContext = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    
    let datePicker = UIDatePicker()
    
    var list = ["Male","Female"]
    
    @IBOutlet weak var Name: UITextField!
    @IBOutlet weak var Age: UITextField!
    @IBOutlet weak var Sex: UITextField!
    @IBOutlet weak var Weight: UITextField!
    
    @IBOutlet weak var dropDown: UIPickerView!

    @IBOutlet var buttonSubmit: UIButton!
    
    
    // MARK: - View Life Cycle
    
    
    override func viewDidLoad() {
        // Do any additional setup after loading the view.
        super.viewDidLoad()
        
        createDatePIcker()
        
        navigationItem.hidesBackButton = true
        
        title = "User Info"
        
        buttonSubmit.layer.cornerRadius = 5
        buttonSubmit.layer.masksToBounds = true
        buttonSubmit.layer.borderColor = UIColor.white.cgColor
        buttonSubmit.layer.borderWidth = 1
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true) //This will hide the keyboard
    }
    
    // MARK: - Private Methods
    
    func createDatePIcker(){
        
        datePicker.datePickerMode = .date
        datePicker.maximumDate = Date()
        //toolbar
        let toolbar = UIToolbar()
        toolbar.sizeToFit()
        //bar button item
        let doneButton = UIBarButtonItem(barButtonSystemItem: .done,target:nil,action: #selector(donePressed))
        
        toolbar.setItems([doneButton], animated: false)
        datePicker.maximumDate = Date()
        Age.inputAccessoryView=toolbar
        Age.inputView=datePicker
    }
    
    
    
    func donePressed(){
        //format date
        
        let datefomatter = DateFormatter()
        datefomatter.dateStyle = .short
        datefomatter.timeStyle = .none
        
        
        Age.text = datefomatter.string(from: datePicker.date)
        self.view.endEditing(true)
    }
    
    // MARK: - UIPickerView Delegate and Data Source Method
    
    public func numberOfComponents(in pickerView: UIPickerView) -> Int{
        return 1
    }
    
    public func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int{
        return list.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        self.view.endEditing(true)
        return list[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        self.Sex.text = self.list[row]
        self.dropDown.isHidden = true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == self.Sex {
            self.dropDown.isHidden = false
            //if you dont want the users to se the keyboard type:
            textField.endEditing(true)
        }
    }
    
    // MARK: - Action Methods
    
    @IBAction func submit(_ sender: Any) {
        // Validate all field before store data on database...
        
        if (Name.text?.characters.count)! > 0 {
            if (Age.text?.characters.count)! > 0 {
                if (Sex.text?.characters.count)! > 0 {
                    if (Weight.text?.characters.count)! > 0 {
                        do {
                            // Strore data on core data
                            let user0 = NSEntityDescription.insertNewObject(forEntityName: "User",   into: managedObjectContext) as! User
                            user0.name = Name.text! as String?
                            user0.age = Age.text!
                            user0.sex = Sex.text!
                            user0.weight = Weight.text!
                            
                            try managedObjectContext.save()
                            UserDefaults.standard.set(true, forKey: "LoggedIn")
                            self.performSegue(withIdentifier: "userHome", sender: nil)
                        } catch {
                            Toast(text: "Something went wrong please try again later.").show()
                        }
                    } else {
                        Toast(text: "Please enter weight!!!").show()
                    }
                } else {
                    Toast(text: "Please enter sex!!!").show()
                }
            } else {
                Toast(text: "Please enter DOB!!!").show()
            }
        } else {
            Toast(text: "Please enter name!!!").show()
        }
    }
    
}



