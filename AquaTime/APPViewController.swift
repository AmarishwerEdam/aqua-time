//
//  APPViewController.swift
//  AquaTime
//
//  Created by Goteti,Santosh Ravi Teja on 4/1/17.
//  Copyright © 2017 Edam,Amarishwer. All rights reserved.
//

import UIKit
import CoreData

class APPViewController: UIViewController, UITableViewDelegate, UITableViewDataSource,UIImagePickerControllerDelegate,
UINavigationControllerDelegate {
    
    // create global constant obj for core data manage object context....
    let managedObjectContext = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    
    @IBOutlet weak var dataTable: UITableView!
    @IBOutlet weak var textViewMessage: UITextView!
    @IBOutlet weak var textFieldAmount: UITextField!

    @IBOutlet weak var imagePicked: UIImageView!
    
    var imageForKey: String!
    
    @IBOutlet var labelUnit: UILabel!
    
    var drinkType: String!
    var valueUnit: String = "ml"
    
    @IBOutlet var qtyButton: UIButton!
    
    // MARK: - View Life Cycle

    
    override func viewDidLoad() {
        super.viewDidLoad()
        dataTable.isHidden = true
        
        self.addDoneButtonOnKeyboard()
        
        // Do any additional setup after loading the view.
        
        // Register the table view cell class and its reuse id
        dataTable.register(UINib(nibName: qtyTableViewCell.nibName, bundle: nil), forCellReuseIdentifier: qtyTableViewCell.identifier)
        dataTable.tableFooterView = UIView()
        
        // Set Right bar button...
        //navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Set Image", style: .plain, target: self, action: #selector(setBGImage(_:)))
        
        qtyButton.layer.cornerRadius = 5
        qtyButton.layer.masksToBounds = true
        qtyButton.layer.borderColor = UIColor.red.cgColor
        qtyButton.layer.borderWidth = 1
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if let value = UserDefaults.standard.value(forKey: "mugUnit") as! String? {
            labelUnit.text = value
            valueUnit = value
        }
        
        if let image = UserDefaults.standard.value(forKey: drinkType) {
            let data = image as? Data
            imagePicked.image = UIImage.init(data: data!)
        }
        
        imagePicked.image = UIImage.init(named: drinkType)
    }
    
    
    // MARK: - Action Methods

    @IBAction func showDefaultQty(_ sender: AnyObject) {
        
        if valueUnit == "ml" {
            let alertController = UIAlertController(title: "Select Quntity", message: nil, preferredStyle: .alert)
            
            let oneAction = UIAlertAction(title: "100 ml", style: .default) { action in
                self.insertValueOnDatabase(value: "100")
            }
            alertController.addAction(oneAction)
            
            let twoAction = UIAlertAction(title: "150 ml", style: .default) { action in
                self.insertValueOnDatabase(value: "150")
            }
            
            alertController.addAction(twoAction)
            
            let threeAction = UIAlertAction(title: "200 ml", style: .default) { action in
                self.insertValueOnDatabase(value: "200")
            }
            alertController.addAction(threeAction)
            
            let cancelAction = UIAlertAction(title: "Cancel", style: .destructive) { action in
                
            }
            alertController.addAction(cancelAction)
            
            self.present(alertController, animated: true) {
                
            }
        } else {
            let alertController = UIAlertController(title: "Select Quntity", message: nil, preferredStyle: .alert)
            
            let oneAction = UIAlertAction(title: "60 oz", style: .default) { action in
                self.insertValueOnDatabase(value: "60")
            }
            alertController.addAction(oneAction)
            
            let twoAction = UIAlertAction(title: "120 oz", style: .default) { action in
                self.insertValueOnDatabase(value: "120")
            }
            
            alertController.addAction(twoAction)
            
            let threeAction = UIAlertAction(title: "180 oz", style: .default) { action in
                self.insertValueOnDatabase(value: "180")
            }
            alertController.addAction(threeAction)
            
            let cancelAction = UIAlertAction(title: "Cancel", style: .destructive) { action in
                
            }
            alertController.addAction(cancelAction)
            
            self.present(alertController, animated: true) {
                
            }
        }
    }
    
    
    @IBAction func setBGImage(_ sender: AnyObject) {
        let alertController = UIAlertController(title: "Choose an option", message: nil, preferredStyle: .alert)
        
        let GalleryAction = UIAlertAction(title: "Gallery", style: .default) { action in
            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.photoLibrary) {
                let imagePicker = UIImagePickerController()
                imagePicker.delegate = self
                imagePicker.sourceType = UIImagePickerControllerSourceType.photoLibrary;
                imagePicker.allowsEditing = true
                self.present(imagePicker, animated: true, completion: nil)
            } else {
                Toast(text: "Photo Library not available in this device!!!").show()
            }
        }
        alertController.addAction(GalleryAction)
        
        let CameraAction = UIAlertAction(title: "Camera", style: .default) { action in
            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera) {
                let imagePicker = UIImagePickerController()
                imagePicker.delegate = self
                imagePicker.sourceType = UIImagePickerControllerSourceType.camera;
                imagePicker.allowsEditing = false
                self.present(imagePicker, animated: true, completion: nil)
            } else {
                Toast(text: "Camera not available in this device!!!").show()
            }
        }
        alertController.addAction(CameraAction)
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .destructive) { action in
            
        }
        alertController.addAction(cancelAction)
        
        self.present(alertController, animated: true) {
            
        }
    }
    
    
    // MARK: - Private Function
    
    // Here we set data on db...
    func insertValueOnDatabase(value: String) {
        do {
            let qty = NSEntityDescription.insertNewObject(forEntityName: "Quantity",   into: managedObjectContext) as! Quantity
            qty.date = Date()
            qty.typeOfDrink = drinkType
            qty.intakeML = Int(value)!
            if valueUnit == "ml" {
                qty.intakeML = Int(value)!
                
                let conversion: Double = Double(value)!
                qty.intakeOZ = Int(conversion / 3.3814)
            } else {
                qty.intakeOZ = Int(value)!
                
                let conversion: Double = Double(value)!
                qty.intakeML = Int(conversion * 3.3814)
            }
            qty.formatedDate = DateFormatter_AquaTime.formatDateForStore(date: Date())
            try managedObjectContext.save()
        } catch {
            Toast(text: "Something went wrong please try again later.").show()
        }
        
        dataTable.isHidden = false
        dataTable.reloadData()
    }
    

    // MARK: - UIImagePickerController Delegate Method

    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        imagePicked.image = info[UIImagePickerControllerOriginalImage] as? UIImage
        
        UserDefaults.standard.set(UIImagePNGRepresentation(imagePicked.image!), forKey: drinkType)
        
        picker.dismiss(animated: true, completion: nil)
    }
    
    // MARK: - UITableViewDataSource methods
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        // create a new cell if needed or reuse an old one
        let cell = tableView.dequeueReusableCell(withIdentifier: qtyTableViewCell.identifier, for: indexPath) as! qtyTableViewCell
        
        do {
            let fetchRequest:NSFetchRequest<Quantity> = NSFetchRequest(entityName: "Quantity")
            let quantitys =
                try managedObjectContext.fetch(fetchRequest)
            for u in quantitys {
                cell.labelDate?.text = DateFormatter_AquaTime.formatDateForNavigationSubtitle(date: u.date!)
                
                if valueUnit == "ml" {
                    cell.labelqty?.text = "\(u.intakeML) \(valueUnit)"
                } else {
                    cell.labelqty?.text = "\(u.intakeOZ) \(valueUnit)"
                }
                
            }
        } catch {
            print("Error when trying to fetch: \(error)")
        }
        
        return cell
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    // MARK: - Private functions

    
    func addDoneButtonOnKeyboard() {
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: 320, height: 50))
        doneToolbar.barStyle       = UIBarStyle.default
        let flexSpace              = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let done: UIBarButtonItem  = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.done, target: self, action: #selector(doneButtonAction))
        
        var items = [UIBarButtonItem]()
        items.append(flexSpace)
        items.append(done)
        
        doneToolbar.items = items
        doneToolbar.sizeToFit()
        
        textFieldAmount.inputAccessoryView = doneToolbar
    }
    
    func doneButtonAction() {
        let trimmedString = textFieldAmount.text?.trimmingCharacters(in: .whitespaces).replacingOccurrences(of: ",", with: "")
        //let newString = aString.replacingOccurrences(of: " ", with: "+")
        if !(trimmedString?.isEmpty)!, trimmedString != "" {
            do {
                let qty = NSEntityDescription.insertNewObject(forEntityName: "Quantity",   into: managedObjectContext) as! Quantity
                qty.date = Date()
                qty.typeOfDrink = drinkType
                
                if valueUnit == "ml" {
                    qty.intakeML = Int(trimmedString!)!
                    
                    let conversion: Double = Double(trimmedString!)!
                    qty.intakeOZ = Int(conversion * 0.033814)
                } else {
                    qty.intakeOZ = Int(trimmedString!)!
                    
                    let conversion: Double = Double(trimmedString!)!
                    qty.intakeML = Int(conversion / 0.033814)
                }
                
                qty.formatedDate = DateFormatter_AquaTime.formatDateForStore(date: Date())
                try managedObjectContext.save()
            } catch {
                print("error------------")
            }
            
            dataTable.isHidden = false
            dataTable.reloadData()
        }
        
        textFieldAmount.resignFirstResponder()
    }
    
}

