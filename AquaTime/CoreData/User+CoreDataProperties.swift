//
//  User+CoreDataProperties.swift
//  AquaTime
//
//  Created by Saikiran Gandham on 4/2/17.
//  Copyright © 2017 Edam,Amarishwer. All rights reserved.
//

import Foundation
import CoreData


extension User {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<User> {
        return NSFetchRequest<User>(entityName: "User");
    }

    @NSManaged public var age: String?
    @NSManaged public var name: String?
    @NSManaged public var sex: String?
    @NSManaged public var weight: String?
    @NSManaged public var data: NSSet?

}

// MARK: Generated accessors for data
extension User {

    @objc(addDataObject:)
    @NSManaged public func addToData(_ value: Quantity)

    @objc(removeDataObject:)
    @NSManaged public func removeFromData(_ value: Quantity)

    @objc(addData:)
    @NSManaged public func addToData(_ values: NSSet)

    @objc(removeData:)
    @NSManaged public func removeFromData(_ values: NSSet)

}
