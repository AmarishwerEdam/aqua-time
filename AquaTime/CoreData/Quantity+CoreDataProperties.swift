//
//  Quantity+CoreDataProperties.swift
//  AquaTime
//
//  Created by Saikiran Gandham on 4/2/17.
//  Copyright © 2017 Edam,Amarishwer. All rights reserved.
//

import Foundation
import CoreData


extension Quantity {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Quantity> {
        return NSFetchRequest<Quantity>(entityName: "Quantity");
    }

    @NSManaged public var date: Date?
    @NSManaged public var intakeML: Int
    @NSManaged public var intakeOZ: Int
    @NSManaged public var typeOfDrink: String
    @NSManaged public var formatedDate: String
    @NSManaged public var consumpted: User?

}
