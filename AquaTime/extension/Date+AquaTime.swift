//
//  Date+AquaTime.swift
//  AquaTime
//
//  Created by Champ on 16/04/17.
//  Copyright © 2017 Edam,Amarishwer. All rights reserved.
//

import Foundation

class DateFormatter_AquaTime {
    
    static let dateFormatter = DateFormatter()
    
    class func formatDateForNavigationSubtitle(date: Date) -> String {
        // Configure subtitle of navigation bar
        
        dateFormatter.amSymbol = "AM"
        dateFormatter.pmSymbol = "PM"
        dateFormatter.dateFormat = "EEE MMM d • h:mm a"
        
        return dateFormatter.string(from: date)
    }
    
    class func formatDateForStore(date: Date) -> String {
        
        dateFormatter.dateFormat = "MMM YYYY"
        
        return dateFormatter.string(from: date)
    }
    
}
