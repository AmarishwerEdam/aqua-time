//
//  SettingsTableViewController.swift
//  AquaTime
//
//  Created by Edam,Amarishwer on 3/5/17.
//  Copyright © 2017 Edam,Amarishwer. All rights reserved.
//

import UIKit
import UserNotifications
import CoreData

class SettingsTableViewController: UITableViewController, UITextFieldDelegate {

    @IBOutlet weak var notificationSwitch: UISwitch!
    @IBOutlet weak var soundSwitch: UISwitch!
    @IBOutlet weak var doNotDisturbSwitch: UISwitch!
    
    @IBOutlet weak var textReminder: UITextField!
    
    // Create constants for appDelegate class object...
    let delegate = UIApplication.shared.delegate as? AppDelegate
    
    // create global constant obj for core data manage object context....
    let managedObjectContext = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    
    var weight: Int = 0
    
    var valueUnit: String = "ml"
    
    // MARK: - View Life Cycle
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.tableFooterView = UIView()
        
        // Update switch accroding to user last input value...
        notificationSwitch.isOn = UserDefaults.standard.bool(forKey: "HourMode")
        soundSwitch.isOn = UserDefaults.standard.bool(forKey: "soundMode")
        //doNotDisturbSwitch.isOn = UserDefaults.standard.bool(forKey: "doNotDisturbMode")
        
        if let value = UserDefaults.standard.value(forKey: "customTime") {
            textReminder.text = value as? String
        }
        
        do {
            let fetchRequest:NSFetchRequest<User> = NSFetchRequest(entityName: "User")
            let user =
                try managedObjectContext.fetch(fetchRequest)
            for u in user {
                weight = Int(u.weight!)!
                weight = weight/2
                break
            }
        } catch {
            print("Error when trying to fetch: \(error)")
        }
        
        // Add done button above keyboard...
        self.addDoneButtonOnKeyboard()
    }
    
    // MARK: - Private Method
    
    func addDoneButtonOnKeyboard() {
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: 320, height: 50))
        doneToolbar.barStyle       = UIBarStyle.default
        let flexSpace              = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let done: UIBarButtonItem  = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.done, target: self, action: #selector(doneButtonAction))
        
        var items = [UIBarButtonItem]()
        items.append(flexSpace)
        items.append(done)
        
        doneToolbar.items = items
        doneToolbar.sizeToFit()
        
        textReminder.inputAccessoryView = doneToolbar
    }
    
    // MARK: - Action Method
    
    @IBAction func doNotDisturbMethod(_ sender: AnyObject) {
        if !sender.isOn {
            // Remove all local notification if user turn off switch...
            removeAllScheduleNotification()
        } else {
            delegate?.scheduleNotificationForSoundMethod(value: sender.isOn)
        }
        
        UserDefaults.standard.set(sender.isOn, forKey: "doNotDisturbMode")
    }
    
    
    @IBAction func setWaterInTake(_ sender: AnyObject) {
        let alertController = UIAlertController(title: "Add water to drink daily", message: nil, preferredStyle: .alert)
        
        let saveAction = UIAlertAction(title: "Save", style: .default, handler: {
            alert -> Void in
            
            let targetAmount = alertController.textFields![0] as UITextField
            
            if let value = UserDefaults.standard.value(forKey: "mugUnit") as! String? {
                self.valueUnit = value
            }
            
            if self.valueUnit == "ml" {
                UserDefaults.standard.set(targetAmount.text, forKey: "targetAmountML")
                
                let conversion: Double = Double(targetAmount.text!)!
                let intC = Int(conversion / 3.3814)
                
                UserDefaults.standard.set(String(intC), forKey: "targetAmountOZ")
            } else {
                let conversion: Double = Double(targetAmount.text!)!
                let intC = Int(conversion * 3.3814)
                
                UserDefaults.standard.set(String(intC), forKey: "targetAmountML")
                
                UserDefaults.standard.set(targetAmount.text, forKey: "targetAmountOZ")
            }
        })
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .default, handler: {
            (action : UIAlertAction!) -> Void in
            
        })
        
        alertController.addTextField { (textField : UITextField!) -> Void in
            let suggest = String(self.weight)
            textField.placeholder = "Enter Amount (Suggestion \(suggest) oz)"
            textField.keyboardType = .numberPad
        }
        
        alertController.addAction(saveAction)
        alertController.addAction(cancelAction)
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    func doneButtonAction() {
        let trimmedString = textReminder.text?.trimmingCharacters(in: .whitespaces)
        
        if !(trimmedString?.isEmpty)!, trimmedString != "" {
            let min = Int(trimmedString!)
            
            if min! < 10 {
                Toast(text: "Please choose time greter then 9 min.").show()
                return
            } else if min! > 240 {
                Toast(text: "Please choose time less then then 4 hr.").show()
                return
            }
            
            UserDefaults.standard.set(trimmedString, forKey: "customTime")
            
            self.removeAllScheduleNotification()
            delegate?.scheduleNotification(timeInterval: min!)
        }
        
        textReminder.resignFirstResponder()
    }
    
    func removeAllScheduleNotification() -> Void {
        let center = UNUserNotificationCenter.current()
        center.removeAllDeliveredNotifications() // To remove all delivered notifications
        center.removeAllPendingNotificationRequests()
    }

    @IBAction func notificationSwitchMethod(_ sender: AnyObject) {
        
        UserDefaults.standard.set(sender.isOn, forKey: "HourMode")
        
        if !sender.isOn {
            removeAllScheduleNotification()
        } else {
            delegate?.scheduleNotificationForSoundMethod(value: sender.isOn)
        }
    }
    
    @IBAction func soundSwitchMethod(_ sender: AnyObject) {
        UserDefaults.standard.set(sender.isOn, forKey: "soundMode")
        
        removeAllScheduleNotification()
        delegate?.scheduleNotificationForSoundMethod(value: sender.isOn)
    }
    
    @IBAction func notificationFrequencyMethod(_ sender: AnyObject) {
        let alertController = UIAlertController(title: "Notification Frequency", message: nil, preferredStyle: .alert)
        
        let thirtyMinAction = UIAlertAction(title: "30 min", style: .default) { action in
            self.removeAllScheduleNotification()
            
            self.delegate?.scheduleNotification(timeInterval: 30)
        }
        alertController.addAction(thirtyMinAction)

        let oneHrAction = UIAlertAction(title: "1 hr", style: .default) { action in
            self.removeAllScheduleNotification()
            
            self.delegate?.scheduleNotification(timeInterval: 60)
        }
        
        alertController.addAction(oneHrAction)
        
        let twoHrAction = UIAlertAction(title: "2 hr", style: .default) { action in
            self.removeAllScheduleNotification()
            
            let delegate = UIApplication.shared.delegate as? AppDelegate
            delegate?.scheduleNotification(timeInterval: 120)

        }
        alertController.addAction(twoHrAction)
        
        let threeHrAction = UIAlertAction(title: "3 hr", style: .default) { action in
            self.removeAllScheduleNotification()
            
            self.delegate?.scheduleNotification(timeInterval: 180)
        }
        alertController.addAction(threeHrAction)
        
        let fourHrAction = UIAlertAction(title: "4 hr", style: .default) { action in
            self.removeAllScheduleNotification()
            
            self.delegate?.scheduleNotification(timeInterval: 240)
        }
        alertController.addAction(fourHrAction)

        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { action in
            
        }
        alertController.addAction(cancelAction)
        
        self.present(alertController, animated: true) {
            
        }
    }
    
    @IBAction func unitsOfMug(_ sender: AnyObject) {
        let alertController = UIAlertController(title: "unit of Mug", message: nil, preferredStyle: .alert)
        
        let literAction = UIAlertAction(title: "ml", style: .default) { action in
            UserDefaults.standard.set("ml", forKey: "mugUnit")
        }
        alertController.addAction(literAction)
        
        let ouncesAction = UIAlertAction(title: "oz", style: .default) { action in
            UserDefaults.standard.set("oz", forKey: "mugUnit")
        }
        alertController.addAction(ouncesAction)
        
        self.present(alertController, animated: true) {
            
        }
    }
    
    @IBAction func signOutMethod(_ sender: AnyObject) {
        if let bundle = Bundle.main.bundleIdentifier {
            KRProgressHUD.show()
            
            // Remove presistent data...
            UserDefaults.standard.removePersistentDomain(forName: bundle)
            
            for key in Array(UserDefaults.standard.dictionaryRepresentation().keys) {
                UserDefaults.standard.removeObject(forKey: key)
            }
            
            // Remove all value from DB...
            deleteAllData(entity: "Quantity")
            deleteAllData(entity: "User")
            
            perform(#selector(closeApp), with: nil, afterDelay: 1.0)
        }
    }
    
    func closeApp() -> Void {
        exit(0)
    }
    
    // Delete all data from coredata...
    func deleteAllData(entity: String) {
        // Create Fetch Request
        let fetchRequestDelete = NSFetchRequest<NSFetchRequestResult>(entityName: entity)
        
        // Create Batch Delete Request
        let batchDeleteRequest = NSBatchDeleteRequest(fetchRequest: fetchRequestDelete)
        
        do {
            try managedObjectContext.execute(batchDeleteRequest)
            
        } catch {
            // Error Handling
        }
    }
    
}
