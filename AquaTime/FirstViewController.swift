//
//  FirstViewController.swift
//  AquaTime
//
//  Created by Edam,Amarishwer on 3/5/17.
//  Copyright © 2017 Edam,Amarishwer. All rights reserved.
//

import UIKit
import CoreData


class FirstViewController: UIViewController {

    @IBOutlet var labelTarget: UILabel!
    @IBOutlet var labelCompleted: UILabel!

    // Create constants for appDelegate class object...
    let appDelegate = UIApplication.shared.delegate as? AppDelegate
    
    // create global constant obj for core data manage object context....
    let managedObjectContext = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    
    var records: [Quantity] = []
    var completed: Int = 0
    
    var valueUnit: String = "ml"
    
    
    
    // MARK: - View Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if let value = UserDefaults.standard.value(forKey: "mugUnit") as! String? {
            valueUnit = value
        }
        
        // Here we check daily target set or not...
        // If not set then on view appears 1st we ask to user to set daily target...
        if !(appDelegate?.isKeyPresentInUserDefaults(key: "targetAmountML"))! {
            setWaterInTake()
        } else {
            // If user already set target then we show on UI...
            
            if self.valueUnit == "ml" {
                if let value = UserDefaults.standard.value(forKey: "targetAmountML") as! String? {
                    labelTarget.text = "\(value) \(valueUnit)"
                }
            } else {
                if let value = UserDefaults.standard.value(forKey: "targetAmountOZ") as! String? {
                    labelTarget.text = "\(value) \(valueUnit)"
                }
            }
        }
        
        // Create Fetch Request
        let fetchRequest:NSFetchRequest<Quantity> = NSFetchRequest(entityName: "Quantity")
        
        // Add Sort Descriptor
        let sortDescriptor = NSSortDescriptor(key: "formatedDate", ascending: true)
        fetchRequest.sortDescriptors = [sortDescriptor]
        
        // Add Predicate
        let query = DateFormatter_AquaTime.formatDateForStore(date: Date())
        let predicate = NSPredicate(format: "formatedDate CONTAINS[c] %@", query)
        fetchRequest.predicate = predicate
        
        do {
            records = try managedObjectContext.fetch(fetchRequest)
            completed = 0
            for record in records {
                if valueUnit == "ml" {
                    completed += record.intakeML
                } else {
                    completed += record.intakeOZ
                }
            }
            
            labelCompleted.text = "\(String(completed)) \(valueUnit)"
        } catch {
            print(error)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Action Methods
    
    @IBAction func beer(_ sender: Any) {
        let a=storyboard?.instantiateViewController(withIdentifier: "APPViewController") as! APPViewController
        a.view.backgroundColor=UIColor.yellow
        a.drinkType = "beer"
        a.textViewMessage.text="Beer is consumably not good, but 2 beers twice a week helps you get better"
        navigationController?.pushViewController(a, animated: true)
    }
    
    
    @IBAction func Juice(_ sender: Any) {
        let a=storyboard?.instantiateViewController(withIdentifier: "APPViewController") as! APPViewController
        a.view.backgroundColor=UIColor.orange
        a.drinkType = "juice"
        a.textViewMessage.text="Drink as much juice as you can as it keeps you healthy"
        navigationController?.pushViewController(a, animated: true)
    }
    
    
    @IBAction func Water(_ sender: Any) {
        let a=storyboard?.instantiateViewController(withIdentifier: "APPViewController") as! APPViewController
        a.view.backgroundColor=UIColor.cyan
        a.drinkType = "water"
        a.textViewMessage.text="Water.. The most essential part.. you will defenitely need to drink it 4 to 5 glasses a Day."
        navigationController?.pushViewController(a, animated: true)
    }
    
    
    @IBAction func Coffee(_ sender: Any) {
        let a=storyboard?.instantiateViewController(withIdentifier: "APPViewController") as! APPViewController
        a.view.backgroundColor=UIColor.brown
        a.drinkType = "coffee"
        a.textViewMessage.text="Coffee makes you feel drowsy, but is a good start during the early hours of the day"
        navigationController?.pushViewController(a, animated: true)
    }
    
    // Set daily water limit to presistent data...
    @IBAction func setWaterInTake() {
        let alertController = UIAlertController(title: "Add water to drink daily", message: nil, preferredStyle: .alert)
        
        let saveAction = UIAlertAction(title: "Save", style: .default, handler: {
            alert -> Void in
            
            let targetAmount = alertController.textFields![0] as UITextField
            
            if self.valueUnit == "ml" {
                UserDefaults.standard.set(targetAmount.text, forKey: "targetAmountML")
                
                let conversion: Double = Double(targetAmount.text!)!
                let intC = Int(conversion / 3.3814)

                UserDefaults.standard.set(String(intC), forKey: "targetAmountOZ")
            } else {
                let conversion: Double = Double(targetAmount.text!)!
                let intC = Int(conversion * 3.3814)
                
                UserDefaults.standard.set(String(intC), forKey: "targetAmountML")
                
                UserDefaults.standard.set(targetAmount.text, forKey: "targetAmountOZ")
            }
            
            if let value = targetAmount.text {
                self.labelTarget.text = "\(value) \(self.valueUnit)"
            }
        })
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .default, handler: {
            (action : UIAlertAction!) -> Void in
            
        })
        
        alertController.addTextField { (textField : UITextField!) -> Void in
            textField.placeholder = "Enter Amount"
            textField.keyboardType = .numberPad
        }
        
        alertController.addAction(saveAction)
        alertController.addAction(cancelAction)
        
        self.present(alertController, animated: true, completion: nil)
    }
}

