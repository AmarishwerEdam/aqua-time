//
//  SecondViewController.swift
//  AquaTime
//
//  Created by Edam,Amarishwer on 3/5/17.
//  Copyright © 2017 Edam,Amarishwer. All rights reserved.
//

import UIKit
import CoreData

class SecondViewController: UIViewController, ChartDelegate {

    // Create some global var and constant...
    @IBOutlet weak var chart: Chart!
    var selectedChart = 0
    
    let formatter = DateFormatter()
    
    @IBOutlet var dropMonth: UIDropDown!
    @IBOutlet var dropYear: UIDropDown!
    
    var labelsAsStringML: Array<String> = []
    var labelsAsStringOZ: Array<String> = []
    
    var valueUnit: String = "ml"
    var records: [Quantity] = []
    
    var month: String = ""
    var year: String = ""
    
    // create global constant obj for core data manage object context....
    let managedObjectContext = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    
    
    // MARK: - View Life Cycle
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        // set data on controllers...
        createMonthDropDown()
        createYearDropDown()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if let value = UserDefaults.standard.value(forKey: "mugUnit") as! String? {
            valueUnit = value
        }
        
        labelsAsStringML.append("100 ml")
        labelsAsStringML.append("200 ml")
        labelsAsStringML.append("300 ml")
        labelsAsStringML.append("400 ml")
        labelsAsStringML.append("500 ml")
        
        labelsAsStringOZ.append("100 oz")
        labelsAsStringOZ.append("200 oz")
        labelsAsStringOZ.append("300 oz")
        labelsAsStringOZ.append("400 oz")
        labelsAsStringOZ.append("500 oz")
        
        // Fetch value forom database....
        getRecordFromDataBase()
    }
    
    // MARK: - Private Methods
    
    func createMonthDropDown() -> Void {
        
        formatter.dateFormat = "MMM"
        month = formatter.string(from: Date())
        
        dropMonth.placeholder = month
        dropMonth.hideOptionsWhenSelect = true
        dropMonth.options = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"]
        dropMonth.didSelect { (option, index) in
            self.month = option
            self.getRecordFromDataBase()
        }
    }
    
    func createYearDropDown() -> Void {
        
        formatter.dateFormat = "YYYY"
        year = formatter.string(from: Date())
        
        dropYear.placeholder = year
        dropYear.hideOptionsWhenSelect = true
        dropYear.options = ["2007", "2008", "2009", "2010", "2011", "2012", "2013", "2014", "2015", "2016", "2017"]
        dropYear.didSelect { (option, index) in
            self.year = option
            self.getRecordFromDataBase()
        }
    }
    
    
    func getRecordFromDataBase() -> Void {
        records = []
        
        // Create Fetch Request
        let fetchRequest:NSFetchRequest<Quantity> = NSFetchRequest(entityName: "Quantity")
        
        // Add Sort Descriptor
        let sortDescriptor = NSSortDescriptor(key: "formatedDate", ascending: true)
        fetchRequest.sortDescriptors = [sortDescriptor]
        
        // Add Predicate
        let query = "\(month) \(year)"
        let predicate = NSPredicate(format: "formatedDate CONTAINS[c] %@", query)
        fetchRequest.predicate = predicate
        
        do {
            records = try managedObjectContext.fetch(fetchRequest)
            
            if records.count <= 0 {
                Toast(text: "No record found for \(month) \(year).").show()
            } else {
                addSeriesOnChart(qty: records)
            }
        } catch {
            print(error)
        }
    }
    
    
    // Method for update chart on Chart screen....
    func addSeriesOnChart(qty: [Quantity]) -> Void {
        var seriesValue: Array<Float> = []
        
        if valueUnit == "ml" {
            seriesValue.append(Float(0.0))
        } else {
            seriesValue.append(Float(0.0))
        }
        
        for i in records {
            if valueUnit == "ml" {
                seriesValue.append(Float(i.intakeML))
            } else {
                seriesValue.append(Float(i.intakeOZ))
            }
        }
        
        let series = ChartSeries(seriesValue)
        series.color = ChartColors.greenColor()
        chart.xLabels = [0,5,10,15,20,25,30]
        chart.yLabels = [100,200,300,400,500]
        if valueUnit == "ml" {
            chart.yLabelsFormatter = { (labelIndex: Int, labelValue: Float) -> String in
                return self.labelsAsStringML[labelIndex]
            }
        } else {
            chart.yLabelsFormatter = { (labelIndex: Int, labelValue: Float) -> String in
                return self.labelsAsStringOZ[labelIndex]
            }
        }
        
        chart.removeAllSeries()
        chart.add(series)
        chart.setNeedsDisplay()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Chart Delegate and Datasource
    
    public func didEndTouchingChart(_ chart: Chart) {
        
    }

    // Chart delegate
    
    func didTouchChart(_ chart: Chart, indexes: Array<Int?>, x: Float, left: CGFloat) {
        for (seriesIndex, dataIndex) in indexes.enumerated() {
            if let value = chart.valueForSeries(seriesIndex, atIndex: dataIndex) {
                print("Touched series: \(seriesIndex): data index: \(dataIndex!); series value: \(value); x-axis value: \(x) (from left: \(left))")
            }
        }
    }
    
    func didFinishTouchingChart(_ chart: Chart) {
        
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        
        super.viewWillTransition(to: size, with: coordinator)
        
        // Redraw chart on rotation
        chart.setNeedsDisplay()
        
    }

}

